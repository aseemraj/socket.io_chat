#Socket.io Chat:
A basic chat application using node and [socket.io](http://socket.io/)

#Requirements:
* [Node.js](http://nodejs.org)
* [Express](http://expressjs.com/)
* [Socket.io](http://socket.io/)

#Installing Requirements:
```
$ npm install express
$ npm install socket.io
```

To make the http server listen on port 3000, cd to the directory and do:
```
$ node index.js
```
==============
